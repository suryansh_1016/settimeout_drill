const boardData = require("./data/boards_1.json");

const getBoard = require("./callback1.cjs");
const getList = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

function getInfo(name, stone) {

    setTimeout(() => {

        const board = boardData.find(element => element.name === name);
        const boardId = board.id;

        getBoard(boardId, (data) => {
            console.log(data);

            getList(boardId, (data) => {
                console.log(data);

                const mind = data.find(element => element.name === stone);
                const mindId = mind.id;

                getCards(boardId, (data) => {
                    console.log(data[mindId]);
                })
            })
        })

    }, 2000)
}

module.exports = getInfo;