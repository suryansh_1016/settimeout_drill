const dataCards = require("./data/cards_1.json");
const dataLists = require("./data/lists_1.json")


function problem3(id, callback) {
    setTimeout(() => {
        const data = {}
        const listItem = dataLists[id];
        for (let item of listItem) {
            if (dataCards[item.id]) {
                data[item.id] = dataCards[item.id]
            }
        }
        callback(data);
    }, 2000)
}

module.exports = problem3