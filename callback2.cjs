const listData = require("./data/lists_1.json");

function problem2(boardId, callback) {
    setTimeout(() => {
        for (let keys in listData) {
            if (boardId === keys) {
                callback(listData[keys]);
            }
        }
    }, 2000)
}

module.exports = problem2;

