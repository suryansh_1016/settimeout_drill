const board = require("./data/boards_1.json");

function problem1(boardId, callback) {
    setTimeout(() => {

        const result = board.find(board => board.id === boardId);
        callback(result);

    }, 2000);
}

module.exports = problem1;


